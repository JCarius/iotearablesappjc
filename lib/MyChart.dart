/// Example of a simple line chart.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleLineChart(this.seriesList, {this.animate});

  /// Creates a [LineChart] with sample data and no transition.
  factory SimpleLineChart.withSampleData() {
    return new SimpleLineChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.LineChart(seriesList, animate: animate);
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<int, int>> _createSampleData() {
    final data = [
      12,
      4,
      3,
      2,
    ];

    return [
      new charts.Series<int, int>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (int sales, int x) => x,
        measureFn: (int sales, _) => sales,
        data: data,
      )
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int y;

  LinearSales(this.y);
}

class SensorElement {
  final int index;
  final double value;
  SensorElement(this.index, this.value);
}