import 'package:flutter/material.dart';

class NewWindow extends StatelessWidget {

  final VoidCallback onComeBack;
  //final Function(int, String) myFunction;
  NewWindow({this.onComeBack}) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("My second window")),
      body: Center(
        child: RaisedButton(
          onPressed: this.onComeBack,
          child: Text(
              "I'm the second window",
              style: Theme.of(context).textTheme.display1,
          ),
        )
      )
    );
  }
}