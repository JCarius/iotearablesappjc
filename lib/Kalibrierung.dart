import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:esense_flutter/esense.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:vector_math/vector_math.dart';
// ignore: non_constant_identifier_names
typedef void ReturnListOfVec3(List<Vector3> l);
typedef void ReturnPos(int p);
class Calibration extends StatefulWidget {
  final ReturnListOfVec3 onCalibrated;
  final ReturnPos onCalibration;
  Calibration({this.onCalibrated, this.onCalibration}) : super();
  @override
  _CalibrationState createState() => _CalibrationState(onCalibrated: onCalibrated, onCalibration: onCalibration);
}
class _CalibrationState extends State<Calibration> {
  final ReturnListOfVec3 onCalibrated;
  final ReturnPos onCalibration;
  bool _calibrationFinished = false;
  Vector3 _leftThreshold= Vector3.zero();
  Vector3 _topThreshold= Vector3.zero();
  Vector3 _rightThreshold= Vector3.zero();
  Vector3 _bottomThreshold= Vector3.zero();
  Vector3 _basisPosition= Vector3.zero();
  int _calibrationIndex = 0;
  String _state = ' ';
  StreamSubscription _buttonSub;
  StreamSubscription _acclAndGyrSub;
  Vector3 _acc = new Vector3(0, 0, 0);
  Vector3 _gyro = new Vector3(0, 0, 0);
  Stopwatch _sw = Stopwatch();
  double _pitch = 0;
  double _roll = 0;
  double _yaw = 0;
  _CalibrationState({this.onCalibrated, this.onCalibration}) : super();
  @override
  void initState() {
    //_listenToESenseEvents();
    _setCalibrationIndex(0);
    super.initState();
  }
  void dispose() {
    if (_calibrationFinished) {
      var threshold = 10;
      if (_leftThreshold.x.abs()  < threshold) _leftThreshold.x = 0;
      if (_leftThreshold.y.abs()  < threshold) _leftThreshold.y = 0;
      if (_leftThreshold.z.abs()  < threshold) _leftThreshold.z = 0;
      if (_topThreshold.x.abs()  < threshold) _topThreshold.x = 0;
      if (_topThreshold.y.abs()  < threshold) _topThreshold.y = 0;
      if (_topThreshold.z.abs()  < threshold) _topThreshold.z = 0;
      if (_rightThreshold.x.abs()  < threshold) _rightThreshold.x = 0;
      if (_rightThreshold.y.abs()  < threshold) _rightThreshold.y = 0;
      if (_rightThreshold.z.abs()  < threshold) _rightThreshold.z = 0;
      if (_bottomThreshold.x.abs()  < threshold) _bottomThreshold.x = 0;
      if (_bottomThreshold.y.abs()  < threshold) _bottomThreshold.y = 0;
      if (_bottomThreshold.z.abs()  < threshold) _bottomThreshold.z = 0;
      onCalibrated([
        _basisPosition,
        _leftThreshold,
        _topThreshold,
        _rightThreshold,
        _bottomThreshold
      ]);
    }
    if (_buttonSub != null)
      _buttonSub.cancel();
    if (_acclAndGyrSub != null)
      _acclAndGyrSub.cancel();
    super.dispose();
  }
  void _reset() {
    _calibrationFinished = false;
    _setCalibrationIndex(0);
  }
  void _setCalibrationIndex(int index) {
    if (index < 0 ||index > 5) return;
    setState(() {
      _calibrationIndex = index;
    });
  }
  void _listenToESenseEvents() async {

    _sw.start();
    _sw.reset();
    _acclAndGyrSub = ESenseManager.sensorEvents.listen((event) {
      //print('SENSOR event: $event');

      setState(() {
        //_event = event.toString();
        _acc.x = event.accel[0].toDouble();
        _acc.y = event.accel[1].toDouble();
        _acc.z = event.accel[2].toDouble();
        _gyro.x = event.gyro[0].toDouble();
        _gyro.y = event.gyro[1].toDouble();
        _gyro.z = event.gyro[2].toDouble();
        _gyro = _gyro/65.5;
        _acc = _acc/8196*9.81;
        //print("sensor event: accl: $accl1, $accl2, $accl3, gyro: $gyroX, $gyroY, $gyroZ");
      });
      _filterSensorEvent();
      //doComplementaryFilter();
      _sw.start();
      _sw.reset();
    });
    _buttonSub = ESenseManager.eSenseEvents.listen((event) {
        if (event.runtimeType == ButtonEventChanged) {
          if ((event as ButtonEventChanged).pressed) {
            _onButtonPressed();
          }
        }
    });


  }
  void _onButtonPressed() {
    onCalibration(_calibrationIndex);
    switch(_calibrationIndex) {
      case 0: _basisPosition = Vector3(_pitch,_roll,_yaw);_setCalibrationIndex(1); break;
      case 1: _leftThreshold = (Vector3(_pitch,_roll,_yaw)-_basisPosition)/2;_setCalibrationIndex(2); break;
      case 2: _bottomThreshold = (Vector3(_pitch,_roll,_yaw)-_basisPosition)/2;_setCalibrationIndex(3); break;
      case 3: _rightThreshold = (Vector3(_pitch,_roll,_yaw)-_basisPosition)/2;_setCalibrationIndex(4); break;
      case 4: _topThreshold = (Vector3(_pitch,_roll,_yaw)-_basisPosition)/2;_setCalibrationIndex(5); _calibrationFinished = true; break;
      case 5: break; //nothing
      default: _state = "Kalibrierung abgebrochen";
    }
  }
  void _filterSensorEvent() {
    double pitchAcc;
    double rollAcc;
    double yawAcc;
    double timescale;
    if (_sw.elapsedMilliseconds > 0) {
      timescale = 0.001 * _sw.elapsedMilliseconds;
    }
    _sw.reset();
    _sw.start();
    // Integrate the gyroscope data -> int(angularSpeed) = angle
    _pitch += _gyro.x *  timescale; // Angle around the X-axis
    _roll -= _gyro.y *  timescale;    // Angle around the Y-axis
    _yaw += _gyro.z * timescale;  //Angle around the Z-axis

    // Kompensation für Ungenauigkeiten und Rauschen
    double force = _acc.x.abs() + _acc.y.abs() + _acc.z.abs();
    //if (force > 8192 && force < 32768) {
    // Drehung um x achse
    pitchAcc = atan2(_acc.y, _acc.z) * 180 / pi;
    _pitch = _pitch * 0.98 + pitchAcc * 0.02;

    // Drehung um y achse
    rollAcc = atan2(_acc.x, _acc.y) * 180 / pi;
    _roll = _roll * 0.98 + rollAcc * 0.02;

    //Drehung um z Achse
    yawAcc = atan2(_acc.x,_acc.z) * 180 / pi;
    _yaw = _yaw * 0.98 + yawAcc * 0.02;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("eSense Kalibrierung"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.autorenew),
            onPressed: () => _reset(),
          ),
        ],
      ),
      body: Center(
        child: Center(
          child: new ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(20.0),
              children: [
                Center(child: new Text("Bitte den Kopf gerade halten und den eSense Knopf drücken.",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 0 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Center(child: new Text("", textScaleFactor: 1.5, textAlign: TextAlign.center,)),
                Center(child: new Text("Bitte den Kopf nach Links lehnen und den eSense Knopf drücken.",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 1 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Center(child: new Text("", textScaleFactor: 1.5, textAlign: TextAlign.center,)),
                Center(child: new Text("Bitte den Kopf nach Vorne lehnen und den eSense Knopf drücken.",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 2 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Center(child: new Text("", textScaleFactor: 1.5, textAlign: TextAlign.center,)),
                Center(child: new Text("Bitte den Kopf nach Rechts lehnen und den eSense Knopf drücken.",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 3 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Center(child: new Text("", textScaleFactor: 1.5, textAlign: TextAlign.center,)),
                Center(child: new Text("Bitte den Kopf nach Hinten lehnen und den eSense Knopf drücken.",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 4 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Center(child: new Text("", textScaleFactor: 1.5, textAlign: TextAlign.center,)),
                Center(child: new Text("Kalibrierung abgeschlossen",
                    textScaleFactor: 1.5, textAlign: TextAlign.center,
                    style: TextStyle(color: _calibrationIndex == 5 ? Theme.of(context).textTheme.display2.backgroundColor : Theme.of(context).backgroundColor))),
                Text('acc: \t$_acc'),
                Text('gyroX: \t$_gyro'),
                Text('pitch: \t$_pitch'),
                Text('roll: \t$_roll'),
                Text('yaw: \t$_yaw'),
                RaisedButton(
                  onPressed: _onButtonPressed,
                  child: Text("Open List"),
                ),
              ]
          ),
        ),
      ), //Text(_state),
    );
  }
}