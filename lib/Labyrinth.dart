import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart';

class LabyrinthNode {
  Vector2 _position;
  LabyrinthNode _left;
  LabyrinthNode _top;
  LabyrinthNode _right;
  LabyrinthNode _bottom;
  LabyrinthNode(Vector2 position) {
    this._position = position;
  }
  void setLeft(LabyrinthNode left) {
    this._left = left;
  }
  void setTop(LabyrinthNode top) {
    this._top = top;
  }
  void setRight(LabyrinthNode right) {
    this._right = right;
  }
  void setBottom(LabyrinthNode bottom) {
    this._bottom = bottom;
  }
  LabyrinthNode left() {
    return _left;
  }
  LabyrinthNode top() {
    return _top;
  }
  LabyrinthNode right() {
    return _right;
  }
  LabyrinthNode bottom() {
    return _bottom;
  }
  Vector2 position() {
    return _position;
  }
}
class Edge {
  Vector2 _start;
  Vector2 _end;
  Edge (start, end) {
    this._start = start;
    this._end = end;
  }
  Vector2 getStart() {
    return _start;
  }
  Vector2 getEnd() {
    return _end;
  }
}
// ignore: must_be_immutable
class Labyrinth extends StatefulWidget{
  List<Vector2> _nodes;
  List<Vector2> _edgeIds;
  int _startNodeIndex;
  int _endNodeIndex;
  _LabyrinthState state;
  bool connected = false;
  Labyrinth(List<Vector2> nodes, List<Vector2> edgeIds, int startNodeIndex, int endNodeIndex) {
    this._edgeIds = edgeIds;
    this._endNodeIndex = endNodeIndex;
    this._nodes = nodes;
    this._startNodeIndex = startNodeIndex;
  }
  void moveLeft() {
    state.moveLeft();
  }
  void moveRight() {
    state.moveRight();
  }
  void moveTop() {
    state.moveTop();
  }
  void moveBottom() {
    state.moveBottom();
  }
  void setConnected(bool b) {
    if(connected != b) {
      connected = b;
      state.setConnected(b);
    }
  }

  @override
  State<StatefulWidget> createState() {
    state = _LabyrinthState(_nodes, _edgeIds, _startNodeIndex, _endNodeIndex);
    return state;
  }
}
class _LabyrinthState extends State<Labyrinth> {
  LabyrinthNode _startNode = new LabyrinthNode(Vector2(-1,-1));
  LabyrinthNode _endNode = new LabyrinthNode(Vector2(-1,-1));
  LabyrinthNode _currentNode = new LabyrinthNode(Vector2(-1,-1));
  int gridWidth = 0;
  int gridHeight = 0;
  bool connected = false;
  bool gewonnen = false;
  List<LabyrinthNode> _nodeList = new List<LabyrinthNode>();
  _LabyrinthState(List<Vector2> nodes, List<Vector2> edgeIds, int startNodeIndex, int endNodeIndex) {
    print('start');
    nodes.forEach((entry) {
      LabyrinthNode node = new LabyrinthNode(entry);
      _nodeList.add(node);
      if (entry.x > gridWidth) gridWidth = entry.x.toInt();
      if (entry.y > gridHeight) gridHeight = entry.y.toInt();
    });
    edgeIds.forEach((entry) {
      LabyrinthNode startNode = getNodeByIndex(entry.x.toInt());
      LabyrinthNode endNode = getNodeByIndex(entry.y.toInt());
      if (!(startNode.position() == Vector2(-1,-1)) && !(endNode.position() == Vector2(-1,-1))) {
        if (startNode.position().x == endNode.position().x) {
          if (startNode.position().y > endNode.position().y) {
            startNode.setTop(endNode);
            endNode.setBottom(startNode);
          } else if (startNode.position().y < endNode.position().y) {
            startNode.setBottom(endNode);
            endNode.setTop(startNode);
          } else {
            //error
          }
        } else if (startNode.position().y == endNode.position().y) {
          if (startNode.position().x > endNode.position().x) {
            startNode.setLeft(endNode);
            endNode.setRight(startNode);
          } else if (startNode.position().x < endNode.position().x) {
            startNode.setRight(endNode);
            endNode.setLeft(startNode);
          } else {
            //error
          }
        } else {
          //error
        }
      }
    });
    _startNode = _nodeList[startNodeIndex];
    _endNode = _nodeList[endNodeIndex];
    _currentNode = _nodeList[startNodeIndex];
    print(_endNode.position().toString());
  }
  LabyrinthNode getNode(Vector2 position) {
    LabyrinthNode node = new LabyrinthNode(Vector2(-1,-1));
    _nodeList.forEach((entry) {
      if (entry.position() == position) {
        node =  entry;
      }
    });
    return node;
  }
  LabyrinthNode getNodeByIndex(int index) {
    if (index < 0 || index >= _nodeList.length)
      return new LabyrinthNode(Vector2(-1,-1));
    return _nodeList[index];
  }
  void moveLeft() {
    if (gewonnen) return;
    if (_currentNode != null && _currentNode.left() != null) {
      setState(() {
        _currentNode = _currentNode.left();
        if (_currentNode.position() == _endNode.position())
          gewonnen = true;
      });
    }
  }
  void moveRight() {
    if (gewonnen) return;
    if (_currentNode != null && _currentNode.right() != null) {
      setState(() {
        _currentNode = _currentNode.right();
        if (_currentNode.position() == _endNode.position())
          gewonnen = true;
      });
    }
  }
  void moveTop() {
    if (gewonnen) return;
    if (_currentNode != null && _currentNode.top() != null) {
      setState(() {
        _currentNode = _currentNode.top();
        if (_currentNode.position() == _endNode.position())
          gewonnen = true;
      });
    }
  }
  void moveBottom() {
    if (gewonnen) return;
    if (_currentNode != null && _currentNode.bottom() != null) {
      setState(() {
        _currentNode = _currentNode.bottom();
        if (_currentNode.position() == _endNode.position())
          gewonnen = true;
      });
    }
  }
  void setConnected(bool b) {
    setState(() {

      connected = b;
    });

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: CustomPaint(
          painter: LabyrinthPainter(gridWidth, gridHeight, _startNode, _currentNode,_endNode, connected),
          child: Container(
            height: 700,
          ),
        ),
      ),
    );
  }

}
class LabyrinthPainter extends CustomPainter  {
  int _gridWidth = 0;
  int _gridHeight = 0;
  LabyrinthNode _startNode;
  LabyrinthNode _currentNode;
  LabyrinthNode _endNode;
  double _cellWidth = 0;
  double _cellHeight = 0;
  double _lineWidth = 0;
  double _circleRadius = 0;
  Color _lineColor = Color(0xffffffff);
  Color _circleColor = Color(0xff0303ae);
  bool connected = false;
  LabyrinthPainter(int w, int h, LabyrinthNode start, LabyrinthNode current, LabyrinthNode end, bool connected) {
    this._gridHeight = h;
    this._gridWidth = w;
    this._startNode = start;
    this._currentNode = current;
    this._endNode = end;
    this.connected = connected;
    print(_endNode.position().toString());
  }
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    // set the paint color to be white
    paint.color = Color(0xFFFFFFFF);
    if (!connected) {
      drawNotConnected(canvas,paint);
      return;
    }
    if (_currentNode.position() == _endNode.position()) {
      drawGewonnen(canvas, paint);
      return;
    }
    // Create a rectangle with size and width same as the canvas
    var rect = Rect.fromLTWH(0, 0, size.width, size.height);
    _cellWidth = size.width / _gridWidth;
    _cellHeight = size.height / _gridHeight;
    _lineWidth = _cellWidth * 2/3;
    _circleRadius = _cellWidth * 1/4;
    // draw the rectangle using the paint
    paint.color = _lineColor;
    paint.strokeWidth = _lineWidth;
    paint.strokeCap = StrokeCap.square;
    drawGridLine(canvas, paint,  _startNode , 0);
    paint.color = Color(0xffb71c1c);
    drawTarget(canvas, paint, _endNode);
    paint.color = _circleColor;
    drawPlayer(canvas, paint, _currentNode);
  }
  void drawPlayer (Canvas canvas, Paint paint, LabyrinthNode node) {

    //print('drawplayer: ' + node.position().toString());
    canvas.drawCircle(Offset(node.position().x*_cellWidth,node.position().y*_cellHeight), _circleRadius, paint);
  }
  void drawTarget (Canvas canvas, Paint paint, LabyrinthNode node) {
    paint.strokeWidth = 2;
    canvas.drawLine(Offset(node.position().x*_cellWidth - _cellWidth / 6,node.position().y*_cellHeight +_cellHeight / 4),
        Offset(node.position().x*_cellWidth- _cellWidth / 6,node.position().y*_cellHeight), paint);
    canvas.drawRect(Rect.fromPoints(Offset(node.position().x*_cellWidth - _cellWidth / 6-1,node.position().y*_cellHeight -_cellHeight / 4),
        Offset(node.position().x*_cellWidth+ _cellWidth / 6,node.position().y*_cellHeight)), paint);
  }
  void drawGewonnen (Canvas canvas, Paint paint) {
    final textStyle = ui.TextStyle(
      color: Color(0xff43A047),
      fontSize: 30,
    );
    final paragraphStyle = ui.ParagraphStyle(
      textDirection: TextDirection.ltr,
    );
    final paragraphBuilder = ui.ParagraphBuilder(paragraphStyle)
      ..pushStyle(textStyle)
      ..addText('Gewonnen!');
    final constraints = ui.ParagraphConstraints(width: 300);
    final paragraph = paragraphBuilder.build();
    paragraph.layout(constraints);
    final offset = Offset(20, 100);
    canvas.drawParagraph(paragraph, offset);
  }
  void drawNotConnected (Canvas canvas, Paint paint) {
    final textStyle = ui.TextStyle(
      color: Color(0xffe53935),
      fontSize: 30,
    );
    final paragraphStyle = ui.ParagraphStyle(
      textDirection: TextDirection.ltr,
    );
    final paragraphBuilder = ui.ParagraphBuilder(paragraphStyle)
      ..pushStyle(textStyle)
      ..addText('Not connected to eSense Device!');
    final constraints = ui.ParagraphConstraints(width: 300);
    final paragraph = paragraphBuilder.build();
    paragraph.layout(constraints);
    final offset = Offset(20, 100);
    canvas.drawParagraph(paragraph, offset);
  }
  void drawGridLine(Canvas canvas, Paint paint, LabyrinthNode node, int dir) { //dir: top 0, right 1, bottom 2, left 3
    if (node == null) return;
    if (node.right() != null && dir != 1) {
      canvas.drawLine(Offset(node.position().x*_cellWidth,node.position().y*_cellHeight),
          Offset(node.right().position().x*_cellWidth,node.right().position().y*_cellHeight), paint);
      drawGridLine(canvas, paint,  node.right() , 3);
    }
    if (node.top() != null && dir != 0) {
      canvas.drawLine(Offset(node.position().x*_cellWidth,node.position().y*_cellHeight),
          Offset(node.top().position().x*_cellWidth,node.top().position().y*_cellHeight), paint);
      drawGridLine(canvas, paint,  node.top(), 2);
    }
    if (node.bottom() != null && dir != 2) {
      canvas.drawLine(Offset(node.position().x*_cellWidth,node.position().y*_cellHeight),
          Offset(node.bottom().position().x*_cellWidth,node.bottom().position().y*_cellHeight), paint);
      drawGridLine(canvas, paint,  node.bottom() , 0);
    }
    if (node.left() != null && dir != 3) {
      canvas.drawLine(Offset(node.position().x*_cellWidth,node.position().y*_cellHeight),
          Offset(node.left().position().x*_cellWidth,node.left().position().y*_cellHeight), paint);
      drawGridLine(canvas, paint,  node.left() , 1);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }

}