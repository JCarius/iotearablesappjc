import 'package:vector_math/vector_math.dart';

class LevelDesign {
  List<Vector2> level1Nodes = [
    Vector2(9,0), //1
    Vector2(8,0), //2
    Vector2(8,1), //3
    Vector2(5,1), //4
    Vector2(3,1), //5
    Vector2(3,0), //6
    Vector2(7,0), //7
    Vector2(1,1), //8
    Vector2(5,2), //9
    Vector2(6,2), //10
    Vector2(9,2), //11
    Vector2(9,1), //12
    Vector2(9,4), //13
    Vector2(8,4), //14
    Vector2(8,5), //15
    Vector2(7,5), //16
    Vector2(7,3), //17
    Vector2(8,3), //18
    Vector2(7,6), //19
    Vector2(5,6), //20
    Vector2(6,3), //21
    Vector2(6,5), //22
    Vector2(4,5), //23
    Vector2(4,3), //24
    Vector2(4,2), //25
    Vector2(0,2), //26
    Vector2(0,0), //27
    Vector2(2,0), //28
    Vector2(0,5), //29
    Vector2(1,5), //30
    Vector2(1,8), //31
    Vector2(0,8), //32
    Vector2(0,6), //33
    Vector2(1,3), //34
    Vector2(1,4), //35
    Vector2(3,4), //36
    Vector2(5,4), //37
    Vector2(3,5), //38
    Vector2(2,5), //39
    Vector2(2,8), //40
    Vector2(2,9), //41
    Vector2(0,9), //42
    Vector2(3,8), //43
    Vector2(3,6), //44
    Vector2(4,6), //45
    Vector2(4,7), //46
    Vector2(4,9), //47
    Vector2(3,9), //48
    Vector2(8,7), //49
    Vector2(8,6), //50
    Vector2(9,6), //51
    Vector2(9,5), //52
    Vector2(9,9), //53
    Vector2(6,9), //54
    Vector2(8,8), //55
    Vector2(5,8), //56
    Vector2(5,9), //57
  ];
  List<Vector2> level1Edges = [
    Vector2(0,1),
    Vector2(1,2),
    Vector2(2,3),
    Vector2(3,4),
    Vector2(4,5),
    Vector2(5,6),
    Vector2(4,7),
    Vector2(3,8),
    Vector2(8,9),
    Vector2(9,10),
    Vector2(10,11),
    Vector2(10,12),
    Vector2(12,13),
    Vector2(13,14),
    Vector2(14,15),
    Vector2(15,16),
    Vector2(16,17),
    Vector2(15,18),
    Vector2(18,19),
    Vector2(9,20),
    Vector2(20,21),
    Vector2(21,22),
    Vector2(20,23),
    Vector2(23,24),
    Vector2(24,25),
    Vector2(25,26),
    Vector2(26,27),
    Vector2(25,28),
    Vector2(28,29),
    Vector2(29,30),
    Vector2(30,31),
    Vector2(31,32),
    Vector2(23,33),
    Vector2(33,34),
    Vector2(34,35),
    Vector2(35,36),
    Vector2(35,37),
    Vector2(37,38),
    Vector2(38,39),
    Vector2(39,40),
    Vector2(40,41),
    Vector2(39,42),
    Vector2(42,43),
    Vector2(43,44),
    Vector2(44,45),
    Vector2(45,46),
    Vector2(46,47),
    Vector2(45,48),
    Vector2(48,49),
    Vector2(49,50),
    Vector2(50,51),
    Vector2(50,52),
    Vector2(52,53),
    Vector2(48,54),
    Vector2(54,55),
    Vector2(55,56),
  ];

  List<Vector2> getLevel1Nodes() {
    return level1Nodes;
  }
  List<Vector2> getLevel1Edges() {
    return level1Edges;
  }
}