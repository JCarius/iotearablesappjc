import 'dart:async';
import 'package:flutter/material.dart';
import 'package:esense_flutter/esense.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:esense/Kalibrierung.dart';

class DevicesList extends StatefulWidget {
  final VoidCallback onDeviceConnected;
  final VoidCallback onDeviceDisconnected;
  final ReturnListOfVec3 onCalibrated;
  final ReturnPos onCalibration;
  DevicesList({this.onDeviceConnected, this.onDeviceDisconnected, this.onCalibrated, this.onCalibration}) : super();
  @override
  _DevicesListState createState() => _DevicesListState(onDeviceConnected: onDeviceConnected, onDeviceDisconnected: onDeviceDisconnected, onCalibrated: onCalibrated, onCalibration: onCalibration);
}
class _DevicesListState extends State<DevicesList> {
  final VoidCallback onDeviceConnected;
  final VoidCallback onDeviceDisconnected;
  final ReturnListOfVec3 onCalibrated;
  final ReturnPos onCalibration;
  _DevicesListState({this.onDeviceConnected, this.onDeviceDisconnected, this.onCalibrated, this.onCalibration}) : super();
  @override
  void initState() {
    super.initState();
    scanSub = FlutterBlue.instance.scanResults.listen((results) {
      // do something with scan results
      for (ScanResult r in results) {

        if (r.device.name.contains('eSense')) {
          _addDevice(r.device);
          if (ESenseManager.eSenseDeviceName == r.device.name && ESenseManager.connected) {
            connectedDevice = r.device;
            _deviceStatus = 'connected';
          }
          //_disconnectDevice(r.device);
        }
      }
    });
    _scanForDevices();
  }
  void dispose() {
    scanSub.cancel();
    connectionSub.cancel();
    t1.cancel();
    super.dispose();
  }

  //final Function(int, String) myFunction;
  List<BluetoothDevice> devicesList = [];
  String _deviceStatus = '';
  BluetoothDevice connectedDevice; //-1 no connection
  StreamSubscription scanSub, connectionSub;
  bool scanning = false;
  Timer t1;
  Future<void> _disconnectDevice(BluetoothDevice device) async {
    //device.disconnect();
    onDeviceDisconnected();
    await ESenseManager.disconnect();
    print('disconnected: $device.name');
    setState(() {
      connectedDevice = null;
    });
  }
  Future<void> _connectDevice(BluetoothDevice device) async {
    if (connectedDevice != null) {
      _disconnectDevice(connectedDevice);
    }
    _connectToESense(device);
    //await device.connect();
    setState(() {
      connectedDevice = device;
    });
  }
  Future<void> _connectToESense(BluetoothDevice device) async {
    bool con = false;

    // if you want to get the connection events when connecting, set up the listener BEFORE connecting...
    connectionSub = ESenseManager.connectionEvents.listen((event) {
      print('CONNECTION event: $event');

      // when we're connected to the eSense device, we can start listening to events from it
      if (event.type == ConnectionType.connected) {
        onDeviceConnected();
        //Navigator.push(context,MaterialPageRoute(builder: (context) => Calibration(onCalibrated: onCalibrated, onCalibration: onCalibration,)));

      }
      /*if (event.type == ConnectionType.disconnected)
        onDeviceDisconnected();*/

      setState(() {
        switch (event.type) {
          case ConnectionType.connected:
            _deviceStatus = 'connected';
            break;
          case ConnectionType.unknown:
            _deviceStatus = 'unknown';
            break;
          case ConnectionType.disconnected:
            _deviceStatus = 'disconnected';
            break;
          case ConnectionType.device_found:
            _deviceStatus = 'device_found';
            break;
          case ConnectionType.device_not_found:
            _deviceStatus = 'device_not_found';
            break;
        }
      });
    });

    con = await ESenseManager.connect(device.name);


    setState(() {
      _deviceStatus = con ? 'connecting' : 'connection failed';
    });
  }

  void _addDevice(BluetoothDevice dev) {
    for (BluetoothDevice e in devicesList) {
      if (e.name == dev.name) {
        return;
      }
    }
    setState(() {
      devicesList.add(dev);
    });
  }

  void _scanForDevices() {
    FlutterBlue flutterBlue = FlutterBlue.instance;
    setState(() {
      devicesList.clear();
      scanning = true;
    });
    flutterBlue.startScan(timeout: Duration(seconds: 2));
    t1 = Timer(Duration(seconds:2), () {setState(() {
      scanning = false;
    });});
    // Listen to scan results



    // Stop scanning
    //flutterBlue.stopScan();
  }
  Future<void> _onDeviceTapped(BluetoothDevice device) async {
    connectedDevice != null ? print('try: $device.name ,con: $connectedDevice.name') : print('try: null');
    if (connectedDevice != null && connectedDevice.id == device.id) {
      _disconnectDevice(device);
    } else {
      await _connectDevice(device);
    }
  }
  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(BluetoothDevice device) => ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        onTap: () {_onDeviceTapped(device); },
        title: Text(
          device.name,
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Row(
          children: <Widget>[
            Icon(Icons.linear_scale),
            Text(connectedDevice != null && connectedDevice.id == device.id ? _deviceStatus : "Verfügbar"),
          ],
        ),
        trailing:
        Icon(Icons.keyboard_arrow_right, size: 30.0)
    );
    Card makeCard(BluetoothDevice device) => Card (
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        child: makeListTile(device),
      ),
    );
    final makeBody = Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: devicesList.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(devicesList[index]);
        },
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(connectedDevice != null ? connectedDevice.name : "eSense Device"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.autorenew),
            onPressed: scanning ? null : _scanForDevices,
          ),
          /*IconButton(
            icon: Icon(Icons.print),
            onPressed: () => Navigator.push(context,MaterialPageRoute(builder: (context) => Calibration(onCalibrated: onCalibrated, onCalibration: onCalibration,))),
          ),*/
        ],
      ),
      body: makeBody,
    );
  }
}