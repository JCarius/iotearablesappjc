import 'package:esense/LevelDesign.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math';

//import 'package:flutter/services.dart';
import 'package:esense_flutter/esense.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:esense/NewWindow.dart';
import 'package:esense/DevicesList.dart';
import 'package:vector_math/vector_math.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:esense/MyChart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'Labyrinth.dart';
void main() => runApp(MatApp());
// berechnung der device orientierung: http://www.pieter-jan.com/node/11
class MatApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "iot app",
      theme: ThemeData.dark(),
      home: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FlutterBlue flutterBlue = FlutterBlue.instance;
  LevelDesign _levelDesign = new LevelDesign();
  String _deviceName = 'Unknown';
  String _button = 'not pressed';
  String _status = 'nothing';
  double _voltage = -1;
  String _deviceStatus = '';
  bool sampling = false;
  //String _event = '';
  Vector3 acc = new Vector3(0, 0, 0);
  Vector3 gyro = new Vector3(0, 0, 0);
  Vector3 acct = new Vector3(0, 0, 0);
  Vector3 gyrot = new Vector3(0, 0, 0);

  double pitch = 0;
  double roll = 0;
  double yaw = 0;
  double pitcht = 0;
  double rollt = 0;
  double yawt = 0;
  String _event = '';
  Stopwatch sw = Stopwatch();
  double zOrient = 0;
  StreamSubscription acclAndGyrSub;
  StreamSubscription buttonSub, sub, subscription;
  DateTime timeLastSensorEvent;
  // the name of the eSense device to connect to -- change this to your own device.
  String eSenseName = 'eSense-0569';
  List<double> pitchData = [0.0,1.0,-1.0];
  List<double> rollData = [0.0,1.0,-1.0];
  List<double> yawData = [0.0,1.0,-1.0];
  List<charts.Series<double, int>> myChartsList;
  Labyrinth lab;
  int currentDirectionType = 0; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
  int lastDirectionType = 0;
  Vector3 currentDirection = new Vector3(0, 1, 0);
  Quaternion posT = Quaternion(1, 0, 0, 0);
  List<Vector3> lastvalues = [Vector3.zero()];//,Vector3.zero(),Vector3.zero(),Vector3.zero(),Vector3.zero()];

  Stopwatch _sw = Stopwatch();
  bool _isCalibrated = false;
  Vector3 _leftThreshold = Vector3.zero();
  Vector3 _topThreshold = Vector3.zero();
  Vector3 _rightThreshold = Vector3.zero();
  Vector3 _bottomThreshold = Vector3.zero();
  Vector3 _basisPosition = Vector3.zero();
  
  bool isConnected = false;


 void onCalibrated(List<Vector3> l) {
   //_basisPosition = l[0];
   //_leftThreshold = l[1];
   //_topThreshold = l[2];
   //_rightThreshold = l[3];
   //_bottomThreshold = l[4];
   _isCalibrated = true;
   //_onDeviceConnected();
 }
 void calibrate(int position) {
   switch(position) {
     case 0: _basisPosition = Vector3(pitch,roll,yaw); break;
     case 1: _leftThreshold = (Vector3(pitch,roll,yaw)-_basisPosition)/2; break;
     case 2: _bottomThreshold = (Vector3(pitch,roll,yaw)-_basisPosition)/2; break;
     case 3: _rightThreshold = (Vector3(pitch,roll,yaw)-_basisPosition)/2; break;
     case 4: _topThreshold = (Vector3(pitch,roll,yaw)-_basisPosition)/2; break;
     default:   return;
   }
 }

  void doComplementaryFilter() {
    Quaternion gyroQuaternion;
    Quaternion accBodyQuaternion;
    Quaternion accWorldQuaternion;//I have no idea what this is. Current position?
    Quaternion cfQuaternion; //complementary filter
    Vector3 v;
    Vector3 n;
    double phi; //ϕ
    double alpha = 0.95; //Was ist das?
    Vector3 omega;
    double theta;
    double lastGyroLength = 0;
    Quaternion finalPosition;
    double timescale = 0.100; //20 ms

    if (_sw.elapsedMilliseconds > 0) {
      //timescale = 0.001 * _sw.elapsedMilliseconds;
    }
    _sw.start();
    _sw.reset();


    omega = gyro / sqrt(gyro.length);//gyroscopedata normalized
    theta = omega.length * timescale * alpha;//tempGyro.distanceTo(Vector3.zero()) - lastGyroLength;
    lastGyroLength = gyro.distanceTo(Vector3.zero());
    gyroQuaternion = new Quaternion(cos(theta/2), omega.x*sin(theta/2), omega.y*sin(theta/2), omega.z*sin(theta/2));
    accBodyQuaternion = new Quaternion(0, acc.x, acc.y, acc.z);
    accWorldQuaternion = posT * accBodyQuaternion * posT.conjugated();
    Quaternion anorm = accWorldQuaternion.normalized();
    phi = (1-alpha)*acos(anorm.y);
    v = new Vector3(anorm.x,anorm.y,anorm.z);
    n = new Vector3(0,anorm.y,0).normalized();
    cfQuaternion = new Quaternion(cos(phi/2), n.x*sin(phi/2), n.y*sin(phi/2), n.z*sin(phi/2));
    finalPosition = posT * cfQuaternion; //finale rotation

    //finalPosition.scale(1-alpha);
    //gyroQuaternion.scale(alpha);
    Quaternion q =  gyroQuaternion;// * finalPosition;
    q.normalize();
    posT = q;
    setState(() {
      //currentDirection = q.rotate(currentDirection);

      lastvalues.add(q.rotate(currentDirection));
      lastvalues.removeAt(0);
      Vector3 vec = Vector3.zero();
      lastvalues.forEach((entry) {
        vec += entry;
      });
      print(vec.toString());
      vec.x = vec.x * (1.0/ lastvalues.length);

      print(vec.toString());
      currentDirection = vec;
    });
  }

  @override
  void initState() {
    super.initState();
    //_connectToESense();
    myChartsList = [
      new charts.Series<double, int>(
        id: 'pitchData',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (_, int x) => x,
        measureFn: (double y, _) => y,
        data: pitchData,
      ),
      new charts.Series<double, int>(
        id: 'pitchData',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (_, int x) => x,
        measureFn: (double y, _) => y,
        data: rollData,
      ),
      new charts.Series<double, int>(
        id: 'pitchData',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (_, int x) => x,
        measureFn: (double y, _) => y,
        data: yawData,
      )
    ];
    lab = new Labyrinth(_levelDesign.getLevel1Nodes(), _levelDesign.getLevel1Edges(),0,56);
  }
  void _filterEvent() {

    lastvalues.add(Vector3.copy(gyro));
    while(lastvalues.length > 10)
      lastvalues.removeAt(0);
    if (lastvalues.length<5) return;
    Vector3 avg = Vector3.zero();
    for(Vector3 d in lastvalues)
      avg += d;

    //print(lastvalues.toString());
    avg /= lastvalues.length.toDouble();
    //print(avg);
    int xStatus;
    int zStatus;
    int threshold = 15;
    if (avg.y > threshold) {
      xStatus = -1;
    } else if (avg.y < -threshold) {
      xStatus = 1;
    } else {
      xStatus = 0;
    }
    if (avg.z > threshold) {
      zStatus = 1;
    } else if (avg.z < -threshold) {
      zStatus = -1;
    } else {
      zStatus = 0;
    }
    double varThresh = 800;
    Vector3 vecVar = Vector3.zero();
    for(Vector3 d in lastvalues) {
      vecVar.x += (d.x - avg.x)*(d.x - avg.x);
      vecVar.y += (d.y - avg.y)*(d.y - avg.y);
      vecVar.z += (d.z - avg.z)*(d.z - avg.z);
    }
    vecVar /= lastvalues.length.toDouble();
    //print(vecVar);
    if (zStatus == 0) {
      if (xStatus == 0) {
        if (currentDirectionType != 0) {
          if( vecVar.x < varThresh &&vecVar.y < varThresh && vecVar.z < varThresh) {
            _status = 'mitte';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 0; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            print(_status);
          } else {

          }
        }
      } else if (xStatus > 0) {
        if (currentDirectionType  == 0 ) {
          _status = 'right';
          lastDirectionType = currentDirectionType;
          currentDirectionType = 2; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveRight();
          print(_status);
        }
      } else {
        if (currentDirectionType == 0) {
          _status = 'left';
          lastDirectionType = currentDirectionType;
          currentDirectionType = 1; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveLeft();
          print(_status);
        }
      }
    } else if (zStatus < 0) {
      if (xStatus == 0) {
        if (currentDirectionType  == 0) {
          _status = 'top';
          lastDirectionType = currentDirectionType;
          currentDirectionType = 4; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveTop();
          print(_status);
        }
      } else if (xStatus > 0) {
        if ( avg.y.abs() > avg.z.abs()) {
          if (currentDirectionType  == 0) {
            _status = 'top';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveTop();
            print(_status);
          }
        } else {
          if (currentDirectionType  == 0 ) {
            _status = 'right';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 2; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveRight();
            print(_status);
          }
        }
      } else {
        if ( avg.y.abs() > avg.z.abs()) {
          if (currentDirectionType  == 0) {
            _status = 'top';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveTop();
            print(_status);
          }
        } else {
          if (currentDirectionType == 0) {
            _status = 'left';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 1; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveLeft();
            print(_status);
          }
        }
      }
    } else {
      if (xStatus == 0) {
        if (currentDirectionType  == 0) {
          _status = 'bottom';
          lastDirectionType = currentDirectionType;
          currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveBottom();
          print(_status);
        }
      } else if (xStatus > 0) {
        if ( avg.y.abs() > avg.z.abs()) {
          if (currentDirectionType  == 0) {
            _status = 'bottom';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveBottom();
            print(_status);
          }
        } else {
          if (currentDirectionType  == 0 ) {
            _status = 'right';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 2; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveRight();
            print(_status);
          }
        }
      } else {
        if ( avg.y.abs() > avg.z.abs()) {
          if (currentDirectionType  == 0) {
            _status = 'bottom';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveBottom();
            print(_status);
          }
        } else {
          if (currentDirectionType == 0) {
            _status = 'left';
            lastDirectionType = currentDirectionType;
            currentDirectionType = 1; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
            lab.moveLeft();
            print(_status);
          }
        }
      }
    }
  }

  void _filterSensorEvent() {
    double pitchAcc;
    double rollAcc;
    double yawAcc;
    double tempPitch = pitch;
    double tempRoll = roll;
    double tempYaw = yaw;
    DateTime time = new DateTime.now();
    Duration diff = time.difference(timeLastSensorEvent);
    timeLastSensorEvent = time;
    // Integrate the gyroscope data -> int(angularSpeed) = angle
    tempPitch += gyro.x *  0.001 * diff.inMilliseconds; // Angle around the X-axis
    tempRoll -= gyro.y *  0.001 * diff.inMilliseconds;    // Angle around the Y-axis
    tempYaw += gyro.z  * 0.001 * diff.inMilliseconds;  //Angle around the Z-axis


    // Kompensation für Ungenauigkeiten und Rauschen
    double force = acc.x.abs() + acc.y.abs() + acc.z.abs();
    //if (force > 8192 && force < 32768) {
      // Drehung um x achse
      pitchAcc = atan2(acc.y, acc.z) * 180 / pi;
      tempPitch = tempPitch * 0.98 + pitchAcc * 0.02;

      // Drehung um y achse
      rollAcc = atan2(acc.x, acc.y) * 180 / pi;
      tempRoll = tempRoll * 0.98 + rollAcc * 0.02;

      //Drehung um z Achse
      yawAcc = atan2(acc.x,acc.z) * 180 / pi;
      tempYaw = tempYaw * 0.98 + yawAcc * 0.02;
      String temp;

      if (((_leftThreshold.x > 0 && pitch - _basisPosition.x > _leftThreshold.x)
              || (_leftThreshold.x < 0 && pitch - _basisPosition.x < _leftThreshold.x)
              || _leftThreshold.x == 0)
          && ((_leftThreshold.y > 0 && roll - _basisPosition.y > _leftThreshold.y)
              || (_leftThreshold.y < 0 && roll - _basisPosition.y < _leftThreshold.y)
              || _leftThreshold.y == 0)
          && ((_leftThreshold.z > 0 && yaw - _basisPosition.z > _leftThreshold.z)
              || (_leftThreshold.z < 0 && yaw - _basisPosition.z < _leftThreshold.z)
              || _leftThreshold.z == 0)) {
        if (currentDirectionType != 1) {
          _status = 'left';
          currentDirectionType = 1; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveLeft();
        }
      } else if (((_topThreshold.x > 0 && pitch - _basisPosition.x > _topThreshold.x)
              || (_topThreshold.x < 0 && pitch - _basisPosition.x < _topThreshold.x)
              || _topThreshold.x == 0)
          && ((_topThreshold.y > 0 && roll - _basisPosition.y > _topThreshold.y)
              || (_topThreshold.y < 0 && roll - _basisPosition.y < _topThreshold.y)
              || _topThreshold.y == 0)
          && ((_topThreshold.z > 0 && yaw - _basisPosition.z > _topThreshold.z)
              || (_topThreshold.z < 0 && yaw - _basisPosition.z < _topThreshold.z)
              || _topThreshold.z == 0)) {
        if (currentDirectionType != 4) {
          _status = 'top';
          currentDirectionType = 4; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveTop();
        }
      } else if (((_rightThreshold.x > 0 && pitch - _basisPosition.x > _rightThreshold.x)
              || (_rightThreshold.x < 0 && pitch - _basisPosition.x < _rightThreshold.x)
              || _rightThreshold.x == 0)
          && ((_rightThreshold.y > 0 && roll - _basisPosition.y > _rightThreshold.y)
              || (_rightThreshold.y < 0 && roll - _basisPosition.y < _rightThreshold.y)
              || _rightThreshold.y == 0)
          && ((_rightThreshold.z > 0 && yaw - _basisPosition.z > _rightThreshold.z)
              || (_rightThreshold.z < 0 && yaw - _basisPosition.z < _rightThreshold.z)
              || _rightThreshold.z == 0)) {
        if (currentDirectionType != 2) {
          _status = 'right';
          currentDirectionType = 2; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveRight();
        }
      } else if (((_bottomThreshold.x > 0 && pitch - _basisPosition.x > _bottomThreshold.x)
              || (_bottomThreshold.x < 0 && pitch - _basisPosition.x < _bottomThreshold.x)
              || _bottomThreshold.x == 0)
          && ((_bottomThreshold.y > 0 && roll - _basisPosition.y > _bottomThreshold.y)
              || (_bottomThreshold.y < 0 && roll - _basisPosition.y < _bottomThreshold.y)
              || _bottomThreshold.y == 0)
          && ((_bottomThreshold.z > 0 && yaw - _basisPosition.z > _bottomThreshold.z)
              || (_bottomThreshold.z < 0 && yaw - _basisPosition.z < _bottomThreshold.z)
              || _bottomThreshold.z == 0)) {
        if (currentDirectionType != 3) {
          _status = 'bottom';
          currentDirectionType = 3; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
          lab.moveBottom();
        }
      } else {
        if (currentDirectionType != 0) {
          _status = 'mitte';
          currentDirectionType = 0; //mitte = 0, links = 1, rechts = 2, vorne = 3, hinten = 4
        }
      }

      setState(() {
        pitch = tempPitch;
        roll = tempRoll;
        yaw = tempYaw;
        pitchData.add(pitch);
        rollData.add(roll);
        yawData.add(yaw);
        //pitchChart = Sparkline(data: pitchData, lineColor: Color(0xFFF44336),); // lineColor: Colors.purple,
        //rollChart = Sparkline(data: rollData, lineColor: Color(0xFF1E88E5),);
        //yawChart = Sparkline(data: yawData, lineColor: Color(0xFF7CB342),);
      });

    //}
  }
  void _listenToESenseEvents() async {
    buttonSub = ESenseManager.eSenseEvents.listen((event) {
      //print('ESENSE event: $event');

      setState(() {
        switch (event.runtimeType) {
          case DeviceNameRead:
            _deviceName = (event as DeviceNameRead).deviceName;
            break;
          case BatteryRead:
            _voltage = (event as BatteryRead).voltage;
            break;
          case ButtonEventChanged:
            _button = (event as ButtonEventChanged).pressed ? 'pressed' : 'not pressed';
            if ((event as ButtonEventChanged).pressed) {
              setState(() {
                acct = acc;
                gyrot = gyro;
                pitcht = pitch;
                rollt = roll;
                yawt = yaw;
                pitchData = [0.0,1.0,-1.0];
                rollData = [0.0,1.0,-1.0];
                yawData = [0.0,1.0,-1.0];
                currentDirection = new Vector3(0, 0, 1);
                _basisPosition = Vector3(pitch, roll, yaw);
              });

            }
            break;
          case AccelerometerOffsetRead:
          // TODO
            break;
          case AdvertisementAndConnectionIntervalRead:
          // TODO
            break;
          case SensorConfigRead:
          // TODO
            break;
        }
      });
    });

    //_getESenseProperties();
  }

  void _getESenseProperties() async {
    // get the battery level every 10 secs
    Timer.periodic(Duration(seconds: 10), (timer) async => await ESenseManager.getBatteryVoltage());

    // wait 2, 3, 4, 5, ... secs before getting the name, offset, etc.
    // it seems like the eSense BTLE interface does NOT like to get called
    // several times in a row -- hence, delays are added in the following calls
    Timer(Duration(seconds: 2), () async => await ESenseManager.getDeviceName());
    Timer(Duration(seconds: 3), () async => await ESenseManager.getAccelerometerOffset());
    Timer(Duration(seconds: 4), () async => await ESenseManager.getAdvertisementAndConnectionInterval());
    Timer(Duration(seconds: 5), () async => await ESenseManager.getSensorConfig());
  }


  void _startListenToSensorEvents() async {
    print('object');
    // subscribe to sensor event from the eSense device
    timeLastSensorEvent = new DateTime.now();
    acclAndGyrSub = ESenseManager.sensorEvents.listen((event) {
      //print('SENSOR event: $event');
      if (!isConnected) {
        isConnected = true;
        lab.setConnected(true);
      }
      setState(() {
        //_event = event.toString();
        acc.x = event.accel[0].toDouble();
        acc.y = event.accel[1].toDouble();
        acc.z = event.accel[2].toDouble();
        gyro.x = event.gyro[0].toDouble();
        gyro.y = event.gyro[1].toDouble();
        gyro.z = event.gyro[2].toDouble();
        gyro = gyro/65.5;
        //acc = acc/8196*9.81;
        //print("sensor event: accl: $accl1, $accl2, $accl3, gyro: $gyroX, $gyroY, $gyroZ");
      });
      _filterEvent();
      //_filterSensorEvent();
      //doComplementaryFilter();
      sw.reset();
      sw.start();
    });
    //print("Sub: " + acclAndGyrSub.toString());
    //acclAndGyrSub.onError((e) => print("error: $e"));
    /*setState(() {
      sampling = true;
    });*/
  }
  void _onDeviceConnected() {
    //WidgetsBinding.instance.addPostFrameCallback((_) {
    _listenToESenseEvents();
    //if (!_isCalibrated) return;
    _startListenToSensorEvents();
    Timer(Duration(seconds: 1), _pauseListenToSensorEvents);
    Timer(Duration(seconds: 2), _startListenToSensorEvents);
    //Timer(Duration(seconds:5), _listenToESenseEvents);

    //});
  }
  void _onDeviceDisconnected() {

  }

  void _pauseListenToSensorEvents() async {
    acclAndGyrSub.cancel();
    setState(() {
      sampling = false;
    });
  }

  void dispose() {
    _pauseListenToSensorEvents();
    ESenseManager.disconnect();
    super.dispose();
    /*List<double> data = [1.0, 2.0];
    charts.Series<double, int> series = new charts.Series<double, int>(
      id: "chart",
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (double x, _) => x,
      measureFn: (_, int x) => x,
      data: data,
    );*/
  }


  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('eSense Labyrinth'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.bluetooth),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new DevicesList(onDeviceConnected: _onDeviceConnected, onDeviceDisconnected: _onDeviceDisconnected, onCalibrated: onCalibrated, onCalibration: calibrate,)),//onComeBack: () => Navigator.pop(context),)),
            ),
          ),
        ],
      ),
      body: Align(
        alignment: Alignment.topLeft,
        child: Center (
          child: Container(
            margin: const EdgeInsets.all(10.0),
            width: 300.0,
            height: 300.0,
            child: lab,//Labyrinth(_levelDesign.getLevel1Nodes(), _levelDesign.getLevel1Edges(),0,56),//Sparkline(data: yawData, lineColor: Color(0xFF7CB342),)
          ),
        ),/*Column(
          children: <Widget>[/*
            Flexible(flex: 1, child: ListView(
              children: [
                //Text('eSense Device Status: \t$_deviceStatus'),
                //Text('eSense Device Name: \t$_deviceName'),
                //Text('eSense Battery Level: \t$_voltage'),
                Text('eSense Button Event: \t$_button'),
                //Text(''),
                //Text('acc: \t$acc'),
                //Text('gyroX: \t$gyro'),
                Text('pitch: ' + (gyro).toString()),
                Text('roll: ' + _status),
                Text('yaw: ' + (yaw - _basisPosition.z).toString()),
                //Text(_status, textScaleFactor: 7,),
              ],
            )
            ),*/
            /*Container(
                width: 300.0,
                height: 100.0,
                child: Sparkline(data: rollData, lineColor: Color(0xFF1E88E5),)
            ),
            Container(
                width: 300.0,
                height: 100.0,
                child: Sparkline(data: pitchData, lineColor: Color(0xFF1E88E5),)
            ),
            Container(
                width: 300.0,
                height: 100.0,
                child: Sparkline(data: yawData, lineColor: Color(0xFF1E88E5),)
            ),
            Container(
                width: 300.0,
                height: 20.0,
                child: Text( _leftThreshold.toString()),//Sparkline(data: yawData, lineColor: Color(0xFF7CB342),)
            ),
            Container(
              width: 300.0,
              height: 20.0,
              child: Text( _rightThreshold.toString()),//Sparkline(data: yawData, lineColor: Color(0xFF7CB342),)
            ),*/


            RaisedButton(
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => new DevicesList(onDeviceConnected: _onDeviceConnected, onDeviceDisconnected: _onDeviceDisconnected, onCalibrated: onCalibrated, onCalibration: calibrate,)),//onComeBack: () => Navigator.pop(context),)),
              ),
              child: Text("Open List"),
            ),
          ],
        )*/
      ),
      /*floatingActionButton: new FloatingActionButton(
        // a floating button that starts/stops listening to sensor events.
        // is disabled until we're connected to the device.
        onPressed:
        (!ESenseManager.connected) ? null : (!sampling) ? _startListenToSensorEvents : _pauseListenToSensorEvents,
        tooltip: 'Listen to eSense sensors',
        child: (!sampling) ? Icon(Icons.play_arrow) : Icon(Icons.pause),
      ),*/
    );
  }
}