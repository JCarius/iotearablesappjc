# Meine eSense App

Leider hat das Projekt keine Dokumentation und viele Altlasten (ungenutzte Funktionen, Variablen). Deshalb hier ein Überblick über die wichtigen Bestandteile: 

In der App geht es darum, einen Punkt durch ein Labyrinth mithilfe der Kopfbewegungen zu lenken. 
Die Bediehnung ist dabei leider etwas unintuitiv, da Aufgrund der Nutzung von dem Durchschnitt 
der letzten ausgelesenen Werte in Kombination mit der Varianz, um eine Zusammenhängende Bewegung 
als solche zu registrieren, der Nutzer nach der Ausführung eienr Bewegung eine kurze Zeit stillhalten muss. 
Auch muss die Bewegung entlang der Earablesachsen erfolgen (für eine optimale Erkennung), was vermutlich eher wenig Benutzerfreundlich ist.

Das Programm besteht aus 4 wichtigen .dart Dateien. DevicesList ist für das Verbinden mit dem Device zuständig, 
Labyrinth ist das Spiel, LevelDesign ist das LevelDesign des Labyrinths, das gespielt wird und main kümmert sich um alles andere.

# DevicesList.dart

DevicesList verbindet sich in initState() mit FlutterBlue scanResults, um Bluetooth geräte anzeigen 
zukönnen und diese nach eSense zu filtern, und startet direkt eine Suche nach verfügbaren Geräten.
Gefundene Geräte werden in einem ListView angezeigt und es kann mit ihnen über ein Tapevent eine 
Verbindung aufgebaut werden. Sobald die Verbindung erfolgreich hergestellt wurde, wird onDeviceConnected()
aufgerufen, woraufhin in main.dart die Verbindungen zu den Sensorevents hergestellt wird.

# main.dart

Nachdem die Verbindung zu einem eSense Gerät erfolgreich war, wird in _startListenToSensorEvents() ein Listener für die sensorevents etabliert.
Dieser liest die Gyroskop Daten aus und ruft _filterEvent() auf, wo der aktuelle Bewegungstyp ermittelt wird. Dies geschieht mithilfe des 
Durchschnitts der letzten 10 Gyroskopwerte zur Bewegungsrichtungbestimmung und der Varianz dieser Werte, um das Ende einer Bewegung festzustellen.
Dadurch muss der Nutzer eine kurze Zeit nach der Bewegung stillhalten, bevor er eine neue Beginnt.

# Labyrinth

Das Labyrinthspiel wird in einem Custompainter gezeichnet und die eSense Bewegungen über die Funktionen lab.moveRight(), lab.moveLeft(), lab.moveTop(), lab.moveBottom() übermittelt.
Das Spiel reagiert entsprechend der Labyrinthstruktur auf die eSense Bewegungen.
